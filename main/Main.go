package main

import (
	"github.com/gin-gonic/gin"
)

/* ------ CONCERT ------*/ 
func displayConcert(ctx *gin.Context) {
	// TODO Etape 2
}

func deleteConcertById(ctx *gin.Context)  {
	// id, erreur := strconv.Atoi(ctx.Param("id")); // Permet de récupérer l'id dans le context, et de le convertir en entier. Renvoit un entier si réussite, sinon une erreur.
	// TODO Etape 3
}

func main() {
	router := gin.Default()
	router.GET("/concerts", displayConcert); // Permet d'appeler la fonction displayConcert quand on get sur /concerts
	// TODO Etape 3
	router.Run(":8080"); // Ecoute sur le port 8080
}

